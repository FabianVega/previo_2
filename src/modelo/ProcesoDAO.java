/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.Proceso;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Asus
 */
public class ProcesoDAO {
    
    ArrayList<String[]> tasklist = new ArrayList<>();
    HashMap<String, Proceso> list = new HashMap<>();
    String msg ;
    
    public void llenarArray() throws IOException {

        String comando[] = {"cmd.exe", "/c", "tasklist /v /fo csv"};

        BufferedReader buff = this.buffer(comando);

        String linea;
        buff.readLine();
        while ((linea = buff.readLine()) != null) {

            String[] c = linea.split("\",\"");
            
            Collections.sort(tasklist, new Comparator<String[]>() {
            @Override
            public int compare(String[] arr1, String[] arr2) {
                // Comparar los elementos en la posición deseada de los arreglos
                return arr1[0].compareToIgnoreCase(arr2[0]); // Ordenar por orden alfabético del primer elemento
            }
        });
            this.tasklist.add(c);

        }
       
    }
    
    public BufferedReader buffer(String[] comando) throws IOException {
		Process proceso = Runtime.getRuntime().exec(comando);

		BufferedReader buff = new BufferedReader(new InputStreamReader(proceso.getInputStream()));

		return buff;
	}
    
    public HashMap<String,Proceso> hash (){
        
        for (int i = 0; i < this.tasklist.size(); i++) {
            
        String[] c = this.tasklist.get(i);
        c[0] = c[0].replace("\"", "");
        c[4] = c[4].replaceAll(" KB","");
        c[4] = c[4].replace(".", "");
        String[] cput = c[7].split(":");
        int segundos = Integer.parseInt(cput[0]) * 60 * 60;
        segundos += Integer.parseInt(cput[1]) * 60;
        segundos += Integer.parseInt(cput[2]);
        
        Proceso p = new Proceso(c[0], c[1], Integer.parseInt(c[4]), segundos);

        //Si encunetra el mismo proceso suma el tiempo de cpu y el consumo de ram
        if (list.containsKey(p.getName())) {
            //Suma de consumo ram
            int a = list.get(p.getName()).getRam();
            a += p.getRam();
            //Suma de tiempo en cpu (en segundos)
            int seg = list.get(p.getName()).getCpu();
            seg += p.getCpu();

            list.get(p.getName()).setRam(a);
            list.get(p.getName()).setCpu(seg);
        } else {
            list.put(p.getName(), p);
        }
        }
        return this.list;
    }
    
    public void eliminar(String pid) throws IOException {
        this.msg = "";
        if(!(pid.equals("0") || pid.equals("4") || pid.equals("944") || pid.equals("2"))){
        if (pid.matches("\\d+")) {
            for (int i = 0; i < this.tasklist.size(); i++) {
                    System.out.println(this.tasklist.size()+" - "+i);
                if (this.tasklist.get(i)[1].equalsIgnoreCase(pid)) {

                    this.tasklist.remove(i);
                    String comando[] = {"cmd.exe", "/c", "taskkill /PID " + pid};
                    Process proceso = Runtime.getRuntime().exec(comando);
                    break;
                    
                }
                System.out.println(this.tasklist.size()+" - "+i);
                if(i ==this.tasklist.size()-1){
                    msg= "The PID you registered is not valid";
                }
            }

        }else{
            msg ="The PID is not a number";
        }
        
        }else{
            msg= "The process should not be closed ";
        }
        
        
        
    }
    
    public ArrayList<String[]> getTasklist() {
        return tasklist;
    }

    public void setTasklist(ArrayList<String[]> tasklist) {
        this.tasklist = tasklist;
    }

    public HashMap<String, Proceso> getList() {
        return list;
    }

    public void setList(HashMap<String, Proceso> list) {
        this.list = list;
    }
    
    public ProcesoDAO() {
        
    }

    public String getMensaje() {
        return msg;
    }


    
        
    
}
