/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.Duration;
import java.time.LocalTime;

/**
 *
 * @author Asus
 */
public class Proceso {
    
    private String name;
    private String pid;
    private int ram;
    private int  cpu;

    public Proceso(String name, String pid, int ram, int cpu) {
        this.name = name;
        this.pid = pid;
        this.ram = ram;
        this.cpu = cpu;
    }
    
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getCpu() {
        return cpu;
    }

    public void setCpu(int cpu) {
        this.cpu = cpu;
    }

    public Proceso() {
    }
    
    public int compareTo(Proceso p) {
        return this.name.compareTo(p.getName());
    }
    
    
    
}
