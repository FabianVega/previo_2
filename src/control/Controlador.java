/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import vista.PageList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Proceso;
import modelo.ProcesoDAO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import vista.PagePlotCpu;
import vista.PagePlotRam;
import vista.Vista;
import vista.WindowKill;

/**
 *
 * @author Asus
 */
public class Controlador implements ActionListener  {

    
    private Vista vista = new Vista();
    private ProcesoDAO proceso = new ProcesoDAO();
    private PageList pageL = new PageList();
    private PagePlotCpu pageCpu = new PagePlotCpu();
    private PagePlotRam pageRam = new PagePlotRam(); 
    private WindowKill wKill = new WindowKill();
    
    

    public Controlador(Vista v){
        try {
            proceso.llenarArray();
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.vista = v;
        this.vista.bList.addActionListener(this);
        this.vista.bRam.addActionListener(this);
        this.vista.bCPU.addActionListener(this);
        this.vista.bKill.addActionListener(this);
        this.wKill.btnKillTask.addActionListener(this);
        this.vista.bUpdate.addActionListener(this);
        
        
    }

    public JPanel listar(JTable tabla) {

        DefaultTableModel modelo = new DefaultTableModel();
        modelo = (DefaultTableModel) tabla.getModel();
        modelo.setRowCount(0);
        ArrayList<String[]> lista = proceso.getTasklist();
        Object[] object = new Object[4];
        for (int i = 0; i < lista.size(); i++) {
            lista.get(i)[0] = lista.get(i)[0].replace("\"", "");
            object[0] = lista.get(i)[0];
            object[1] = lista.get(i)[1];
            object[2] = lista.get(i)[4];
            object[3] = lista.get(i)[7];
            modelo.addRow(object);
        }

        pageL.table.setModel(modelo);
        System.out.println("Si entre aca :)");
        return pageL;
    }

    private void pintarGrafico(DefaultPieDataset datos, String titulo, JPanel p) {
        JFreeChart grafico = ChartFactory.createPieChart(titulo, datos, true, true, false);
        ChartPanel panel = new ChartPanel(grafico);
        panel.setMouseWheelEnabled(true);
        panel.setPreferredSize(new Dimension(800, 800));
        p.setLayout(new BorderLayout());
        p.add(panel, BorderLayout.NORTH);
    }

    public JPanel pintarCPU(JPanel p) {
        DefaultPieDataset datos = new DefaultPieDataset();
        HashMap<String, Proceso> listaHash = proceso.hash();
        for (Map.Entry<String, Proceso> entry : listaHash.entrySet()) {
            String key = entry.getKey();
            datos.setValue(entry.getKey(), entry.getValue().getCpu());
           
        }
        pintarGrafico(datos, "CPU", p);
        return p;
    }

    public JPanel pintarRAM(JPanel p) {
        DefaultPieDataset datos = new DefaultPieDataset();
        HashMap<String, Proceso> listaHash = proceso.hash();
        for (Map.Entry<String, Proceso> entry : listaHash.entrySet()) {
            
            datos.setValue(entry.getKey(), entry.getValue().getRam());
        }
        pintarGrafico(datos, "RAM", p);
        return p;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.bList) {
            
            vista.ShowPanel(this.listar(pageL.table));
        }
        if (e.getSource()==vista.bRam) {
            
            vista.ShowPanel(this.pintarRAM(pageRam));
            
            System.out.println("En el action performed");

            
        }
        if (e.getSource() == vista.bCPU) {
            
            vista.ShowPanel(this.pintarCPU(pageCpu));
            
        }
        if (e.getSource()== vista.bKill) {
            
            wKill.setVisible(true);
            vista.ShowPanel(this.listar(pageL.table));
        }
        
        if (e.getSource()== wKill.btnKillTask  ) {
            
            try {
                proceso.eliminar(wKill.pid.getText());
                wKill.pid.setText("");
                wKill.lbError.setText("");
                this.listar(pageL.table);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

                wKill.lbError.setText(proceso.getMensaje());
            
        }
        if (e.getSource() == vista.bUpdate){
                    try {
            proceso.llenarArray();
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
                    this.pintarCPU(pageCpu);
                    this.pintarRAM(pageRam);
                    this.listar(pageL.table);
        }
    }
}

    
    
    

